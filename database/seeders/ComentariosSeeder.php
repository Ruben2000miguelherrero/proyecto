<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Comentario;
use App\Models\User;

class ComentariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $comentarios=array(

            array("producto_id"=>"1","fecha"=>"2020-01-01","texto"=>"Brutal , echaba de menos una placa sin lucecitas ni chorradas , tiene un aspecto agresivo y robusto sin necesidad de luces . En cuanto a calidad de materiales 10/10 muy bien refrigerado todo "),

            array("producto_id"=>"4","fecha"=>"2020-01-01","texto"=>"Fui de los afortunados de comprarlo al momento. Está montado en una ROG Strix X570-E Gaming. Estoy realizando los primeros test serios con el motor de render Arnold, que es el motor que uso para mis escenarios, y la diferencia respecto a mi anterior CPU son difíciles de creer."),

            array("producto_id"=>"7","fecha"=>"2020-01-01","texto"=>"muy buena ram, para empezar con 8 gb esta muy bien, le puse un disipador aftermarket y perfecto, 2400 mhz que tengo oc en 3400... muy buena"),

            array("producto_id"=>"10","fecha"=>"2020-01-01","texto"=>"Muy buen disco duro, teniendo en cuenta que lo compré en rebajas, me salió por 20 euros menos que el precio normal de los ssd de 480-500gb."),

            array("producto_id"=>"13","fecha"=>"2020-01-01","texto"=>"Nos ha solucionado el problema quebteníamos con gráficos y juegos. Es uma buena solución a un precio asequible pero hace mucho ruido"),

            array("producto_id"=>"16","fecha"=>"2020-01-01","texto"=>"La fuente para mi va perfecta,potencia suficiente para cualquier equipo de gama alta.Puedo overclockear un ryzen 1700 a 4ghz con dos tomas 4+4 y con la antigua (650w/plus bronze,una toma 4+4) no podia pasarlo de 3,8ghz"),

            array("producto_id"=>"16","fecha"=>"2020-01-01","texto"=>"los cables son muy rigidos"),

            array("producto_id"=>"19","fecha"=>"2020-01-01","texto"=>"Nada que no se sepa, buen funcionamiento lectura y escritura sin problemas hasta el momento prefecto."),

            array("producto_id"=>"22","fecha"=>"2020-01-01","texto"=>"Me encanta Lynx. Su diseño, el espacio que tiene para todos los componentes, lo bien refrigerado que está todo el equipo. Un 10!"),

            array("producto_id"=>"25","fecha"=>"2020-01-01","texto"=>"un teclado de lo más barato, pero cumple su función. el tacto de las teclas es muy personal, pero a mi me va muy bien con este.es básico, como el salpicadero de un panda."),

            array("producto_id"=>"28","fecha"=>"2020-01-01","texto"=>"raton simple optico. para los que no necesitamos más que eso, un raton, es idoneo. tenia miedo porque en la foto me parecia raton pequeño como para portatil pero es el raton tradicional de toda la vida pc de sobremesa, grande, pero oye ¿a quién lo le gustan grandes?"),

            array("producto_id"=>"31","fecha"=>"2020-01-01","texto"=>"Buena calidad de imagen.Acabado elegante"),

            array("producto_id"=>"10","fecha"=>"2020-01-01","texto"=>"Gran ssd con buenos niveles de lectura/escritura. Instalar el sistema en uno de estos te hará ganar en salud"),
    );

    public function run()
    {
        foreach ($this->comentarios as $comentario){
			$coment = new Comentario();
            $coment->user_id = User::all()->random()->id;
            $coment->producto_id = $comentario['producto_id'];
            $coment->fecha = $comentario['fecha'];
            $coment->texto = $comentario['texto'];
			$coment->save();
		}
		$this->command->info('Tabla comentarios inicializada con datos');
    }
}
