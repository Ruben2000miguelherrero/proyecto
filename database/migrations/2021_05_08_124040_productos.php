<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Productos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->id();
            $table->string('slug')->unique();
            $table->unsignedBigInteger('categoria_id');
            $table->foreign('categoria_id')->references('id')->on('categorias');
            $table->string('modelo');
            $table->boolean('deFabrica');
            $table->double('precio',6,2);
            $table->integer('stock');
            $table->string('imagen');
            $table->longText('Caracteristicas');
            $table->string('Especificacion1')->nullable();
            $table->string('Especificacion2')->nullable();
            $table->string('Especificacion3')->nullable();
            $table->string('Especificacion4')->nullable();
            $table->string('Funcionalidad');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
