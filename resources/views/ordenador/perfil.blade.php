@extends('layouts.master')
	@section('titulo')
		CompPartes
	@endsection
	
	@section('contenido')
	<body style="background-color: gray">
	<form action="{{route('ordenador.update')}}" method="POST" enctype="multipart/form-data">
		@csrf
		@method('put')
		<br>
		<div class="container">
		<div class="main-body">
			<div class="row">
				<div class="col-lg-4">
					<div class="card">
						<div class="card-body">
							<div class="d-flex flex-column align-items-center text-center">
								<h4>Imagen</h4>
								<img src="<?php echo "assets/imagenes/users/".session('user')->imagen?>" class="rounded-circle p-1 bg-primary" width="280">
								<br>
								<input type="file" class="form-control" name="imagen" id="imagen">
							</div>
							<br>
							<p><strong>Contraseña: <?php echo session('user')->password?></strong></p>
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mb-0">Nueva</h6>
								</div>
								<div class="col-sm-9 text-secondary">
									<input type="password" name="password" id="password" class="form-control" placeholder="Nueva">
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mb-0"></h6>
								</div>
								<div class="col-sm-9 text-secondary">
									<input type="password" name="confirmPass" id="confirmPass" class="form-control" placeholder="Confirmacion">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-8">
					<div class="card">
						<div class="card-body">
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mb-0">Correo electronico</h6>
								</div>
								<div class="col-sm-9 text-secondary">
									<input type="email" class="form-control" name="correo" id="correo" value="<?php echo session('user')->email?>" readonly>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mb-0">Nombre</h6>
								</div>
								<div class="col-sm-9 text-secondary">
									<input type="text" class="form-control" name="nombre" id="nombre"value="<?php echo session('user')->nombre?>" required>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mb-0">Apellidos</h6>
								</div>
								<div class="col-sm-9 text-secondary">
									<input type="text" class="form-control" name="apellidos" id="apellidos" value="<?php echo session('user')->apellidos?>" required>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mb-0">Fecha</h6>
								</div>
								<div class="col-sm-9 text-secondary">
									<input type="date" class="form-control" name="fechaNacimiento" id="fechaNacimiento" value="<?php echo session('user')->fechaNacimiento?>" required>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mb-0">Capital</h6>
								</div>
								<div class="col-sm-9 text-secondary">
									<input type="number" min="0" class="form-control" name="capital" id="capital" value="<?php echo session('user')->capital?>" required>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mb-0">Pais</h6>
								</div>
								<div class="col-sm-9 text-secondary">
									<input type="text" class="form-control" name="pais" id="pais" value="<?php echo session('user')->pais?>" required>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mb-0">Provincia</h6>
								</div>
								<div class="col-sm-9 text-secondary">
									<input type="text" class="form-control" name="provincia" id="provincia" value="<?php echo session('user')->provincia?>" required>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mb-0">Localidad</h6>
								</div>
								<div class="col-sm-9 text-secondary">
									<input type="text" class="form-control" name="localidad" id="localidad" value="<?php echo session('user')->localidad?>" required>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mb-0">Direccion</h6>
								</div>
								<div class="col-sm-9 text-secondary">
									<input type="text" class="form-control" name="direccion" id="direccion" value="<?php echo session('user')->direccion?>" required>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-3"></div>
								<div class="col-sm-9 text-secondary">
									<input type="submit" class="btn btn-success px-4" value="Guardar Cambios">
									<a href="{{route('ordenador.historial')}}" class="btn btn-primary px-4">Historial</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
</form>
</body>
	@endsection