@extends('layouts.master')
	@section('titulo')
		CompPartes
	@endsection
	
	@section('contenido')
		<h1>Componentes</h1>
		<form action="{{route('ordenador.conjunto')}}" method="POST" enctype="multipart/form-data">
			@csrf
			@method('post')
				<?php $x=0;?>
				@foreach($productos as $clave => $producto)
					<input type="hidden" id="conjun[<?php echo $x?>]" name="conjun[<?php echo $x?>]" value="{{$producto->id}}">
					<?php $x++;?>
				@endforeach
			<button type="submit" class="btn btn-success" style="margin-left: 280px; margin-top:-70px">
				Añadir todo el conjunto
			</button>
		</form>
		<div class="row">
			@foreach( $productos as $clave => $producto )
				@if($producto->stock==0)
					<div class="col-xs-12 col-sm-6 col-md-4 border border-danger rounded">
				@else
					<div class="col-xs-12 col-sm-6 col-md-4 ">
				@endif
					<a href="{{route('ordenador.show',$producto)}}">
						<div class="text-center">
							<img class="rounded" src="{{asset('assets/imagenes/productos/')}}/{{$producto->imagen}}" style="height:200px;"/>
						</div>
						<h4 style="min-height:45px;margin:5px 0 10px 0" align="center">
							{{$producto->modelo}}
						</h4>
					</a>
					<ul>
						<div class="container">
							<div class="row">
								<div class="col"><li>Precio: {{$producto->precio}} euros</li></div>
								@if($producto->stock==0)
									<div class="col"><li class="text-danger">Agotado</li></div>
								@else
									@if($producto->deFabrica==1)
										<div class="col"><li>De Fabrica</li></div>
									@else
										<div class="col"><li>De segunda mano</li></div>
									@endif
								@endif
							</div>
						</div>
					</ul>
				</div>
			@endforeach
		</div>

		<h1>Posibles repuestos</h1>
		<div class="row">
			@foreach( $repuestos as $clave => $producto )
				@if($producto->stock==0)
					<div class="col-xs-12 col-sm-6 col-md-4 border border-danger rounded">
				@else
					<div class="col-xs-12 col-sm-6 col-md-4 ">
				@endif
					<a href="{{route('ordenador.show',$producto)}}">
						<div class="text-center">
							<img class="rounded" src="{{asset('assets/imagenes/productos/')}}/{{$producto->imagen}}" style="height:200px;"/>
						</div>
						<h4 style="min-height:45px;margin:5px 0 10px 0" align="center">
							{{$producto->modelo}}
						</h4>
					</a>
					<ul>
						<div class="container">
							<div class="row">
								<div class="col"><li>Precio: {{$producto->precio}} euros</li></div>
								@if($producto->stock==0)
									<div class="col"><li class="text-danger">Agotado</li></div>
								@else
									@if($producto->deFabrica==1)
										<div class="col"><li>De Fabrica</li></div>
									@else
										<div class="col"><li>De segunda mano</li></div>
									@endif
								@endif
							</div>
						</div>
					</ul>
				</div>
			@endforeach
		</div>
	@endsection