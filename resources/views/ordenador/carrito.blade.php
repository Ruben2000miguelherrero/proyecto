@extends('layouts.master')
	@section('titulo')
		CompPartes
	@endsection
	
	@section('contenido')
		<?php
			if(session('carrito')==null){
				echo "<br><ul class='list-group'>
                	<li class='list-group-item list-group-item-danger' style='position:relative; z-index:-1;'>Tu carrito esta vacio.</li>
            	</ul><br>";
			}else{
		?>
		
		<div class="row">
			<?php
				$var=0;
				$total=0;
				foreach(session('carrito') as $clave => $unidad ){	
			?>
		    <div class="col-sm-6">
		    <div class="card">
		      <div class="card-body">
		      	<div class="row" style='position:relative; z-index:0'>
					<div class="col-sm-3">
		      			<img class="rounded mx-auto d-block" src="{{asset('assets/imagenes/productos/')}}/{{$unidad['producto']->imagen}}" style="height:120px"/>
		      		</div>
		      		<div class="col-sm-7">
				        <h5 class="card-title">{{$unidad['producto']->modelo}}</h5>
				        <p class="card-text">Precio:{{$unidad['producto']->precio}}€ &nbsp;&nbsp; Cantidad:{{$unidad['cantidad']}}</p>
				        <form action="{{route('ordenador.quitar')}}" method="POST" enctype="multipart/form-data">
			        	@csrf
						@method('post')
			        		<input type="hidden" name="pos" id="pos" value="{{$var}}" readonly>
			        		<button type="submit" class="btn btn-danger">Eliminar</button>
				        	<?php
				        		$total=$total+($unidad['producto']->precio*$unidad['cantidad']);
				        	?>
		       			</form>
				    </div>
				</div>
		      </div>
		    </div>
		  </div>
		  <?php
		  	$var++;
		  	}
		  ?>

		</div>
		<br>
		<p>Total <?php echo $total;?> €</p>
		@if(session('user')->capital>=$total)
			<a href="{{route('ordenador.confirmar')}}" class="btn btn-success">Confirmar compra</a>
		@else
			<button class="btn btn-danger" disabled="disabled">No se tiene capital suficiente</button>
		@endif
		<?php
		  }
		?>
@endsection