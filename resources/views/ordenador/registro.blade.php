@extends('layouts.master')
	@section('titulo')
		CompPartes
	@endsection
	
	@section('contenido')
        <body style="background-color: gray">
        <br>
		<form action="{{route('ordenador.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('post')
            <div class="offset-md-3 col-md-6">
                <div class="card">
                <div class="card-header text-center">
                    Registro
                </div>
            <div class="card-body">
            <div class="row">
                @if(session()->has('aviso'))
                <ul class="list-group">
                    <li class="list-group-item list-group-item-danger">{{session('aviso')}}</li>
                </ul>
                {{session()->forget('aviso')}}
                @endif
                <div class="col">
                    <label for="nombre">Nombre</label>
                    <input required type="text" name="nombre" id="nombre" class="form-control">
                </div>
                <div class="col">
                    <label for="apellidos">Apellidos</label>
                    <input required type="text" name="apellidos" id="apellidos" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input required type="email" name="email" id="email" class="form-control" placeholder="ejemplo@gmail.com">
            </div>
            <div class="row">
                <div class="col">
                    <label for="pais">Pais</label>
                    <input required type="text" name="pais" id="pais" class="form-control">
                </div>
                <div class="col">
                    <label for="fechaNacimiento">Fecha de nacimiento</label>
                    <input required type="date" name="fechaNacimiento" id="fechaNacimiento" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="provincia">Provincia</label>
                    <input required type="text" name="provincia" id="provincia" class="form-control">
                </div>
                <div class="col">
                    <label for="localidad">Localidad</label>
                    <input required type="text" name="localidad" id="localidad" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="direccion">Direccion</label>
                <input required type="text" name="direccion" id="direccion" class="form-control" placeholder="Introduce tu direccion">
            </div>
            <div class="form-group">
                <label for="password">Contraseña</label>
                <input required type="password" name="password" id="password" class="form-control">
            </div>
            <div class="form-group">
                <label for="confirmPass">Confirma tu contraseña</label>
                <input required type="password" name="confirmPass" id="confirmPass" class="form-control">
            </div>
            <br>
            <button type="submit" name="enviar" class="btn btn-primary form-control">Registrar nuevo usuario</button>
            <a href="{{route('ordenador.intro')}}">Volver al login</a>
            </div>
            </div>
        </div>
        </form>
        </body>
        <br>
	@endsection
