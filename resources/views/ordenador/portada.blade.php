@extends('layouts.master')
	@section('titulo')
		CompPartes
	@endsection
	
	@section('contenido')
		<link href="{{asset('assets/css/centro.css')}}" rel="stylesheet">
		<body style="background-image: url({{asset('assets/imagenes/admin/portada.jpg')}}); background-repeat: no-repeat;background-position: center;background-size: cover;"> 
			<br>
			<div class="container-fluid">
			  <div class="row">
			    <div class="col-xs-12">
			      <img class="rounded-circle" style="border-radius: 10px; box-shadow: 5px 10px 18px #33FFFA; height:150px; position:absolute; margin-left:42%; z-index:-1;" src="{{asset('assets/imagenes/admin/captura.png')}}"/>
			      <br>
			      <br>
			      <br>
			      <br>
			      <br>
			      <br>
				  <p style="font-family:bold; font-size: 40px;color: skyblue; text-align: center;">¡Bienvenido a CompPartes!</p>
			    </div>
			  </div>
			 </div>
		</body>

		<p style="font-family:bold; font-size: 25px; color: #4004C6;"><em>¡Mira nuestras existencias!</em></p>
		<div class="row" style="background-color: white;">
		@foreach( $productos as $clave => $producto )
				@if($producto->stock==0)
					<div class="col-xs-12 col-sm-6 col-md-4 border border-danger rounded">
				@else
					<div class="col-xs-12 col-sm-6 col-md-4 border border-success">
				@endif
					<a href="{{route('ordenador.show',$producto)}}">
						<div class="text-center">
							<img class="rounded" src="{{asset('assets/imagenes/productos/')}}/{{$producto->imagen}}" style="height:200px"/>
						</div>
						<h4 style="min-height:45px;margin:5px 0 10px 0" align="center">
							{{$producto->modelo}}
						</h4>
					</a>
					<ul>
						<div class="container">
							<div class="row">
								<div class="col"><li>Precio: {{$producto->precio}} euros</li></div>
								@if($producto->stock==0)
									<div class="col"><li class="text-danger">Agotado</li></div>
								@else
									@if($producto->deFabrica==1)
										<div class="col"><li>Nuevo</li></div>
									@else
										<div class="col"><li>De segunda mano</li></div>
									@endif
								@endif
							</div>
						</div>
					</ul>
				</div>
			@endforeach
		</div>
	@endsection