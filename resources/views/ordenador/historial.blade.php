@extends('layouts.master')
	@section('titulo')
		CompPartes
	@endsection
	
	@section('contenido')
		<h1>Comentarios</h1>
		@if(sizeof(session('user')->comentarios)>0)
		<table class="table table-striped">
		  <thead>
		    <tr>
		      <th scope="col">Producto</th>
		      <th scope="col">Fecha</th>
		      <th scope="col">Texto</th>
		    </tr>
		  </thead>
		  <tbody>
		  	@foreach(session('user')->comentarios as $clave =>$comentario )
		  		<tr>
			      <td>{{$comentario->producto->modelo}}</td>
			      <td>{{$comentario->fecha}}</td>
			      <td>{{$comentario->texto}}</td>
			    </tr>
		  	@endforeach
		  </tbody>
		</table>
		@else
			<ul class='list-group'>
                <li class='list-group-item list-group-item-danger' style='position:relative; z-index:-1;'>No has colgado ningun comentario.</li>
            </ul>
		@endif


		<h1>Compras</h1>
		@if(sizeof(session('user')->ventas)>0)
		<table class="table table-striped">
		  <thead>
		    <tr>
		      <th scope="col">Producto</th>
		      <th scope="col">Cantidad</th>
		      <th scope="col">Fecha</th>
		      <th scope="col">Precio</th>
		    </tr>
		  </thead>
		  <tbody>
		  	@foreach(session('user')->ventas as $clave =>$venta )
		  		<tr>
			      <td>{{$venta->producto->modelo}}</td>
			      <td>{{$venta->unidades}}</td>
			      <td>{{$venta->fechaCompra}}</td>
			      <td>{{$venta->producto->precio*$venta->unidades}}</td>
			    </tr>
		  	@endforeach
		  </tbody>
		</table>
		@else
			<ul class='list-group'>
                <li class='list-group-item list-group-item-danger' style='position:relative; z-index:-1;'>No has realizado ninguna compra.</li>
            </ul>
		@endif
	@endsection