@extends('layouts.master')
	@section('titulo')
		CompPartes
	@endsection
	
	@section('contenido')
		<br>
		<div class="row">
			<div class="col-sm-3">
				<img class="rounded mx-auto d-block" src="{{asset('assets/imagenes/productos/')}}/{{$producto->imagen}}" style="height:220px"/>
				<h3 style="margin-left: 50px;">{{$producto->precio}} €</h3>
				<br>
				<form action="{{route('ordenador.anadir',$producto)}}" method="POST" enctype="multipart/form-data">
					@csrf
					@method('post')
					<div class="container-fluid">
		    			<div class="row" id="map_section">
		        			<div class="col-4">
								<input type="number" id="cant" name="cant" class="form-control" min="1" value="1" max="{{$producto->stock}}">
		        			</div>
		        			<div class="col-8">
		        				@if($producto->stock>0)
									<button type="submit" class="btn btn-success">Añadir al carrito</button>
								@else
									<button type="submit" class="btn btn-danger" disabled="disabled">No hay existencias</button>
								@endif
		        			</div>
		    			</div>
					</div>
				</form>
			</div>
			<div class="col-sm-9">
				<h4 style="min-height:45px;margin:5px 0 10px 0">{{$producto->modelo}}
					@if($producto->deFabrica==0)
						<span style="color:red;">De segunda mano</span>
					@endif
				</h4>
				<ul style="list-style-type: none; position:absolute; z-index:-1;" class="list-group">
					<li class="list-group-item">Caracteristicas: {{$producto->Caracteristicas}}</li>
				</ul>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<ul style="list-style-type: none; position:absolute; z-index:-1;" class="list-group list-group-flush">
					<li class="list-group-item" style="width: 390px;"><strong>></strong>{{$producto->Especificacion1}}</li>
					<li class="list-group-item" style="width: 390px;"><strong>></strong>{{$producto->Especificacion2}}</li>
					<li class="list-group-item" style="width: 390px;"><strong>></strong>{{$producto->Especificacion3}}</li>
					<li class="list-group-item" style="width: 390px;"><strong>></strong>{{$producto->Especificacion4}}</li>	
				</ul>
			</div>
		</div>
		<br><br>
		<div class="col-auto p-5">
			<form action="{{route('ordenador.coment')}}" method="POST" enctype="multipart/form-data">
			@csrf
			@method('post')
			<div class="input-group">
			  <input type="text" id="coment" name="coment" class="form-control" placeholder="Comentario">
			  <input type="hidden" id="producto" name="producto" value="{{$producto->id}}">
			  <button type="submit" class="btn btn-success btn-sm"><img src="{{asset('assets/imagenes/admin/enviar.png')}}" style="height:25px"/></button>
			</div>
		    </form>
			<br>
		    @foreach($producto->comentarios as $llave => $comentario)
				<table class="table table-bordered table-secondary">
				<tr>
					<td>
					  	<div>
					  		<img class="rounded-circle" src="{{asset('assets/imagenes/users/')}}/{{$comentario->user->imagen}}" style="height:30px"/>
					  		<label class="blockquote">{{$comentario->user->nombre}}</label>              <small>{{$comentario->fecha}}</small>
					  		<br>
					    	<label>{{$comentario->texto}}</label>
				    	</div>
					</td>
				</tr>
			</table>
			@endforeach
		</div>
	@endsection

