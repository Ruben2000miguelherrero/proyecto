<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InicioController;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CategoriaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[InicioController::class,'home'])->name('home');//Pagina de portada

Route::post('/',[UserController::class,'entrar'])//Metodo para meter credenciales
	->name('ordenador.entrar');

Route::get('{numero}/productos',[CategoriaController::class,'porcate'])//Pagina para mostrar los productos por categorias
	->name('ordenador.cate');

Route::get('formulario',[InicioController::class,'formulario'])//Pagina para el formulario
	->name('ordenador.formulario');

Route::post('formulario',[InicioController::class,'componentes'])//Metodo para el formulario
	->name('ordenador.componentes');

Route::get('productos',[ProductoController::class,'index'])//Pagina de muestra de todos losproductos
	->name('ordenador.productos');

Route::get('productos/{producto}',[ProductoController::class,'show'])//Pagina mostrar un producto
	->name('ordenador.show');

Route::post('productos',[ProductoController::class,'coment'])//Metodo para crear un comentario
	->name('ordenador.coment');

Route::get('intro',[UserController::class,'intro'])//Pagina para meter credenciales
	->name('ordenador.intro');

Route::get('registro',[UserController::class,'registro'])//Pagina para registrarse
	->name('ordenador.registro');

Route::post('registro',[UserController::class,'store'])//Metodo para registrarse
	->name('ordenador.store');

Route::get('usuario',[UserController::class,'perfil'])//Pagina mostrar un perfil de usuario
	->name('ordenador.perfil');

Route::get('usuario/carrito',[UserController::class,'carrito'])//Pagina ver el carrito de usuario
	->name('ordenador.carrito');

Route::put('usuario',[UserController::class,'update'])//Metodo para modificar el perfil de un usuario
	->name('ordenador.update');

Route::get('usuario/cerrar',[UserController::class,'cerrar'])//Metodo para cerrar sesion
	->name('ordenador.cerrar');

Route::post('usuario/carrito/{producto}',[ProductoController::class,'anadir'])//Metodo para añadir un producto al carrito
	->name('ordenador.anadir');

Route::post('usuario/carrito',[ProductoController::class,'quitar'])//Metodo para eliminar un producto del carrito
	->name('ordenador.quitar');

Route::get('usuario/carrito/confirmar',[UserController::class,'confirmar'])//Metodo para confirmar la compra
	->name('ordenador.confirmar');

Route::get('usuario/historial',[UserController::class,'historial'])//Pagina para ver tus aportaciones
	->name('ordenador.historial');

Route::post('formulario/conjunto',[ProductoController::class,'conjunto'])//Metodo para añadir el conjunto
	->name('ordenador.conjunto');

Route::post('busqueda',[ProductoController::class,'busqueda'])//Buscador
	->name('ordenador.busqueda');







Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
