<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Producto;

class CategoriaController extends Controller
{
    public function porcate($numero){
        $productos=Producto::where('categoria_id',$numero)->paginate(9);
    	return view('ordenador.productos',['productos'=>$productos]);
    }
}
