<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categoria;
use App\Models\Producto;
use App\Models\User;
use App\Models\Comentario;
use Illuminate\Support\Str;

class ProductoController extends Controller
{
    public function index(){
        $productos=Producto::paginate(9);
    	return view('ordenador.productos',['productos'=>$productos]);
    }

    public function show(Producto $producto){
    	return view('ordenador.show', ['producto'=>$producto]);
    }

    public function coment(Request $request){
        if(session()->has('user')){
            $datos=new Comentario();
            $datos->user_id=session('user')->id;
            $datos->producto_id=$request->producto;
            $datos->fecha=date("Y-m-j");
            $datos->texto=$request->coment;
            $datos->save();
            $prod= Producto::where('id',$request->producto)->first();
            return view('ordenador.show', ['producto'=>$prod]);
        }else{
            session(['aviso' => "Es necesario registrarse para colgar un comentario."]);
            return view('ordenador.intro');
        }
    }

    public function anadir(Producto $producto,Request $request){
        if(session()->has('user')){//Comprobamos si esta registrado
            if(session('carrito')==null){//Comprobamos si el carrito existe
                $lista=array(array("producto"=>$producto,"cantidad"=>$request->cant));
                session(['carrito' =>$lista]);
            }else{
                $var=session('carrito');
                array_push($var, array("producto"=>$producto,"cantidad"=>$request->cant));
                session(['carrito' => $var]);
            }
            return view('ordenador.carrito');
        }else{
            session(['aviso' => "Es necesario registrarse para añadir al carrito."]);
            return view('ordenador.intro');
        }
    }

    public function quitar(Request $request){
        $aux=session('carrito');
        array_splice($aux, $request->pos, 1);
        session(['carrito' =>$aux]);
        return view('ordenador.carrito');
    }

    public function conjunto(Request $request){
        if(session()->has('user')){//Comprobamos si esta registrado
            if(session('carrito')==null){//Comprobamos si el carrito existe
                $pro=array();
                $tamaño=count($request->conjun);
                for($i=0; $i<$tamaño; $i++) {
                    $aux=$request->conjun[$i];
                    $prod= Producto::where('id',$aux)->first();
                    array_push($pro, array("producto"=>$prod,"cantidad"=>1));
                }
                session(['carrito' =>$pro]);
            }else{
                $tamaño=count($request->conjun);
                $var=session('carrito');
                for($i=0; $i<$tamaño; $i++) {
                    $aux=$request->conjun[$i];
                    $prod= Producto::where('id',$aux)->first();
                    array_push($var,array("producto"=>$prod,"cantidad"=>1));
                }
                session(['carrito' => $var]);
            }
            return view('ordenador.carrito');
        }else{
            session(['aviso' => "Es necesario registrarse para añadir al carrito."]);
            return view('ordenador.intro');
        }
    }

    public function busqueda(Request $request){
        $bus="%".$request->busqueda."%";
        $productos=Producto::where('modelo','like',$bus)->paginate(9);
        return view('ordenador.productos',['productos'=>$productos]);
    }
}
