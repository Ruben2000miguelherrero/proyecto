<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Producto;
use App\Models\User;

class Comentario extends Model
{
    use HasFactory;
    protected $table="comentarios";
    protected $guarded=["token"];

    public function producto(){
    	return $this->belongsTo(Producto::class);
    }

    public function user(){
    	return $this->belongsTo(User::class);
    }
}
