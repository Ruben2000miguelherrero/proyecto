<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Categoria;
use App\Models\Comentario;
use App\Models\Venta;

class Producto extends Model
{
    use HasFactory;
    protected $table="productos";
    protected $guarded=["token"];

    public function getRouteKeyName()
	{
		return 'slug';
	}

	public function categoria(){
    	return $this->belongsTo(Categoria::class);
    }

    public function comentarios(){
		return $this->hasMany(Comentario::class);
	}

	public function ventas(){
		return $this->hasMany(Comentario::class);
	}
}
